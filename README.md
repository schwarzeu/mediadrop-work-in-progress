Access restriction for videos
===================

Currently MediaCore displays all videos publicly.

This branch adds infrastructure + API to restrict media views to specific groups. Also you can grant the media view privilege to selected groups per video.

The code contains a lot of infrastructure changes:

- new login form which can display real error messages
 - "forgot password" functionality
- remove repoze.what, use custom authorization layer
- plugin interface so that additional permission policies can be configured
- a very basic "public profile" page where users can change their password

Current State
-------------

- catch permission errors and display a "nice" error page
- policies do not apply for API access (maybe that's ok until get rid of the "API key" and replace it with user logins?)
- podcasts are not protected
