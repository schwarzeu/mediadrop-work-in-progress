# This file is a part of MediaCore CE, Copyright 2009-2012 MediaCore Inc.
# The source code contained in this file is licensed under the GPL.
# See LICENSE.txt in the main project directory, for more information.
#
# Copyright (c) 2012 Felix Schwarz (www.schwarz.eu)

import re
import logging

from mediacore.lib.auth.api import PermissionSystem, UserPermissions
from mediacore.plugin.abc import AbstractClass, abstractmethod
from sqlalchemy import or_


__all__ = ['MediaCorePermissionSystem', 'PermissionPolicies']

log = logging.getLogger(__name__)

class PermissionPolicies(AbstractClass):
    @abstractmethod
    def permits(self, permission, perm, resource):
        pass
    
    @classmethod
    def configured_policies(cls, config):
        policy_names = re.split('\s*,\s*', config.get('permission_policies', ''))
        if policy_names == ['']:
            policy_names = ['GroupBasedPermissionsPolicy']
        return map(cls._policy_from_name, policy_names)
    
    @classmethod
    def _policy_from_name(cls, policy_name):
        for policy in cls:
            if policy.__name__ == policy_name:
                return policy()
        raise AssertionError('No such policy: %s' % repr(policy_name))


class MediaCorePermissionSystem(PermissionSystem):
    def __init__(self, config):
        policies = PermissionPolicies.configured_policies(config)
        super(MediaCorePermissionSystem, self).__init__(policies)
    
    @classmethod
    def permissions_for_request(cls, environ, config):
        user_name = environ.get('repoze.who.identity', {}).get('repoze.who.userid', None)
        perm = UserPermissions(user_name, MediaCorePermissionSystem(config))
        
        # seeding the User object as we need it anyway
        cls._set_user_and_groups(perm)
        return perm
    
    @classmethod
    def _set_user_and_groups(cls, perm):
        # we can not import model classes before the DB setup was done, so
        # just import the class here.
        from mediacore.model import DBSession, Group, User
        def _set_for_group_by_name(group_name):
            group_or_none = DBSession.query(Group).filter(Group.group_name == group_name).first()
            group_set = set([group_or_none])
            return set(filter(None, group_set))
        
        perm.user = None
        perm.groups = _set_for_group_by_name(u'anonymous')
        if perm.user_name is None:
            return
        
        perm.user = User.by_user_name(perm.user_name)
        if perm.user is None:
            perm.user = User.by_email_address(perm.user_name)
            perm.user_name = perm.user.user_name
        perm.groups.update(perm.user.groups)
        perm.groups.update(_set_for_group_by_name(u'authenticated'))
    
    def _gather_speedups(self, query, permission, perm):
        speedups = []
        for policy in self.policies:
            if not hasattr(policy, 'speedups_for_query'):
                policy_name = policy.__class__.__name__
                log.debug(policy_name + ' does not provide speedups for ' + permission)
                speedups.append(None)
                break
            speedups.append(policy.speedups_for_query(query, permission, perm))
        return speedups
    
    def _apply_speedups_to_query(self, speedups, query):
        optimized_query = query
        optional_clauses = []
        for speedup in speedups:
            if speedup.get('optional_condition') is not None:
                optional_clauses.append(speedup['optional_condition'])
            if speedup.get('query') is not None:
                optimized_query = speedup['query']
        if optional_clauses:
            optimized_query = optimized_query.distinct().filter(or_(*optional_clauses))
        return optimized_query
    
    def apply_speedups_to_query(self, query, permission, perm):
        speedups = self._gather_speedups(query, permission, perm)
        if None in speedups:
            return None
        return self._apply_speedups_to_query(speedups, query)


