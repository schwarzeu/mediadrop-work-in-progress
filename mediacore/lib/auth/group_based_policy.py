# This file is a part of MediaCore CE, Copyright 2009-2012 MediaCore Inc.
# The source code contained in this file is licensed under the GPL.
# See LICENSE.txt in the main project directory, for more information.
#
# Copyright (c) 2012 Felix Schwarz (www.schwarz.eu)

from mediacore.lib.auth.api import IPermissionPolicy
from mediacore.lib.auth.permission_system import PermissionPolicies


__all__ = ['GroupBasedPermissionsPolicy']

class GroupBasedPermissionsPolicy(IPermissionPolicy):
    
    def _permissions(self, perm):
        if 'permissions' not in perm.data:
            if perm.groups is None:
                return ()
            permissions = []
            for group in perm.groups:
                permissions.extend([p.permission_name for p in group.permissions])
            perm.data['permissions'] = permissions
        return perm.data['permissions']
    
    def permits(self, permission, perm, resource):
        if permission in self._permissions(perm):
            return True
        # there may be other policies still which can permit the access...
        return None
    
    def speedups_for_query(self, query, permission, perm):
        if permission != 'MEDIA_VIEW':
            return None
        
        from mediacore.model.media import Media
        # condition will always be true, so all media will be returned
        condition = (Media.id > -1)
        if not perm.contains_permission('MEDIA_VIEW'):
            # condition will always be false, so no media will be returned 
            # unless another policy allows it.
            condition = (Media.id == -1)
        return dict(
            optional_condition = condition,
        )

PermissionPolicies.register(GroupBasedPermissionsPolicy)

