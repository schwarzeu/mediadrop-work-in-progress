# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Felix Schwarz <felix.schwarz@oss.schwarz.eu>
#
# This file may be used under the terms of the MIT license (see license text 
# below) or the GNU General Public License as published by the Free Software 
# Foundation, either version 3 of the License, or (at your option) any later 
# version.
# 
# The MIT License
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

__all__ = ['Resource', 'InsufficientPermissionsError', 'IPermissionPolicy', 
    'UserPermissions', 'PermissionSystem']


class Resource(object):
    def __init__(self, realm, id, **kwargs):
        self.realm = realm
        self.id = id
        self.data = dict(kwargs)


class IPermissionPolicy(object):
    def permits(self, permission, user_permissions, resource):
        pass


class InsufficientPermissionsError(Exception):
    def __init__(self, permission, resource=None):
        self.permission = permission
        self.resource = resource


class UserPermissions(object):
    
    def __init__(self, user_name, permission_system):
        self.user_name = user_name
        self.permission_system = permission_system
        self.user = None
        self.data = {}
    
    def assert_permission(self, permission, resource=None):
        self.permission_system.assert_permission(permission, self, resource)
    
    def contains_permission(self, permission, resource=None):
        return self.permission_system.has_permission(permission, self, resource)



class PermissionSystem(object):
    def __init__(self, policies):
        self.policies = tuple(policies)
    
    def assert_permission(self, permission, user_permissions, resource=None):
        decision = self.has_permission(permission, user_permissions, resource)
        if decision == False:
            raise InsufficientPermissionsError(permission, resource)
    
    def has_permission(self, permission, user_permissions, resource=None):
        for policy in self.policies:
            decision = policy.permits(permission, user_permissions, resource)
            if decision in (True, False):
                return decision
        return False

