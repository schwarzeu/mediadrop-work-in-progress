# This file is a part of MediaCore CE, Copyright 2009-2012 MediaCore Inc.
# The source code contained in this file is licensed under the GPL.
# See LICENSE.txt in the main project directory, for more information.

from pylons import request

from mediacore.forms.profile import PasswordChangeForm
from mediacore.lib.auth import is_authenticated
from mediacore.lib.base import BaseController
from mediacore.lib.decorators import (autocommit, expose, observable, validate)
from mediacore.lib.helpers import redirect, url_for
from mediacore.plugin import events


password_change_form = PasswordChangeForm()

class ProfileController(BaseController):
    """
    Profile Controller

    It handles display of user account information and allows to change some
    settings.
    """
    allow_only = is_authenticated()

    @expose('profile/index.html')
    @observable(events.ProfileController.index)
    def index(self, **kwargs):
        """Account overview page.
        :returns:
            user
                The :class:`~mediacore.model.auth.User` instance

        """
        
        return dict(
            user = request.perm.user
        )

    @expose('profile/password_edit.html')
    @observable(events.ProfileController.password_edit)
    def password_edit(self, **kwargs):
        return dict(
            user = request.perm.user,
            form_action = url_for(action='password_save'),
            form = password_change_form,
            form_values = kwargs,
        )

    @expose(request_method='post')
    @validate(password_change_form, error_handler=password_edit)
    @autocommit
    @observable(events.ProfileController.password_save)
    def password_save(self, current_password, new_password, **kwargs):
        user = request.perm.user
        user.password = new_password
        redirect(action='index', id=None)

