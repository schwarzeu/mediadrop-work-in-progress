# This file is a part of MediaCore CE, Copyright 2009-2012 MediaCore Inc.
# The source code contained in this file is licensed under the GPL.
# See LICENSE.txt in the main project directory, for more information.
#
# Copyright (c) 2012 Felix Schwarz (www.schwarz.eu)

from datetime import datetime
import os
import smtplib
import random
import time

from pylons import request, tmpl_context
from pylons.controllers.util import abort, redirect

from mediacore.forms.password import (ForgotPasswordForm, ResetPasswordForm, 
    ValidResetPasswordHashSchema)
from mediacore.lib.base import BaseController
from mediacore.lib.compat import sha1
from mediacore.lib.email import send_password_reset
from mediacore.lib.decorators import autocommit, expose, observable, validate
from mediacore.lib.helpers import redirect, url_for
from mediacore.model import User
from mediacore.plugin import events

import logging
log = logging.getLogger(__name__)


forgot_password_form = ForgotPasswordForm()
reset_password_form = ResetPasswordForm()

class PasswordController(BaseController):
    @expose('password/reset.html')
    @observable(events.PasswordController.reset)
    def reset(self, **kwargs):
        return dict(
            forgot_password_form=forgot_password_form,
            form_values=kwargs,
        )

    def _generate_hash(self, user):
        salt = sha1()
        salt.update(os.urandom(60))
        salt.update(user.password)
        return salt.hexdigest()[:25]

    @expose(request_method='post')
    @validate(forgot_password_form, error_handler=reset)
    @autocommit
    @observable(events.PasswordController.request_reset)
    def request_reset(self, email, **kwargs):
        user = User.by_email_address(email)
        if user is not None:
            hash_ = self._generate_hash(user)
            user.data['password_reset'] = {
                'hash': hash_,
                'created_on': time.mktime(datetime.utcnow().timetuple()),
            }
            reset_url = url_for('/password/confirm_reset', email=email, 
                hash_=hash_, qualified=True)
            try:
                send_password_reset(user, reset_url)
            except smtplib.SMTPException, e:
                log.exception(e)
                # should we notify the user here?
        redirect('/password/reset_sent')

    @expose('password/reset_sent.html')
    @observable(events.PasswordController.reset_sent)
    def reset_sent(self, **kwargs):
        return dict()

    @expose('password/bad_hash.html')
    @observable(events.PasswordController.bad_hash)
    def bad_hash(self, **kwargs):
        return dict()

    # @validate before @expose because otherwse @expose would get only a string
    # as result, which will cause an exception.
    @validate(ValidResetPasswordHashSchema(), error_handler=bad_hash)
    @expose('password/confirm_reset.html')
    @observable(events.PasswordController.confirm_reset)
    def confirm_reset(self, **kwargs):
        return dict(
            forgot_password_form = reset_password_form,
            form_values = kwargs,
        )

    @expose(request_method='post')
    @validate(reset_password_form, error_handler=confirm_reset)
    @autocommit
    @observable(events.PasswordController.reset_password)
    def reset_password(self, email=None, new_password=None, **kwargs):
        user = User.by_email_address(email)
        user.password = new_password
        del user.data['password_reset']
        log.info(u'Reset password for user %s' % unicode(user))
        redirect(controller='password', action='reset_successful')

    @expose('password/reset_successful.html')
    @observable(events.PasswordController.reset_sent)
    def reset_successful(self, **kwargs):
        return dict()

