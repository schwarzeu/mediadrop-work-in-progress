# This file is a part of MediaCore CE, Copyright 2009-2012 MediaCore Inc.
# The source code contained in this file is licensed under the GPL.
# See LICENSE.txt in the main project directory, for more information.
#
# Copyright (c) 2012 Felix Schwarz (www.schwarz.eu)

from pylons import request
from tw.forms import FormField, PasswordField
from tw.forms.validators import (FancyValidator, FieldsMatch, Invalid, 
    NotEmpty, Schema)

from mediacore.forms import LinkifyMixin, ListForm, SubmitButton, TableForm
from mediacore.lib.i18n import N_, _
from mediacore.lib.util import url_for
from mediacore.plugin import events

class ValidPassword(FancyValidator):
    def _to_python(self, value, state):
        user = request.perm.user
        if not user.validate_password(value):
            raise Invalid(_('Invalid Password'), value, state)


class TextLink(FormField, LinkifyMixin):
    params = dict(link='', default='')
    template = u'<a href="${link}" id="${id}" class="${css_class}">${default}</a>'
    # required as this element is technically just a link so it's value won't
    # be in the list of fields sent to the server and the default validator will
    # cause an error.
    validator = FancyValidator(if_missing='')
    
    def update_params(self, d):
        super(TextLink, self).update_params(d)
        # Ensure link is correct if MediaCore is behind a reverse proxy or 
        # installed in a subdirectory.
        d.link = url_for(d.link)


class TextLinkButton(TextLink):
    # actually this isn't a button but declaring it as one so the default form
    # templates will place it at the bottom together with real buttons (e.g.
    # submit)
    type = 'button'
    css_classes = ['mcore-textlink-button']


class PasswordChangeForm(ListForm):
    template = 'forms/box_form.html'
    method = 'POST'
    id = 'password-change-form'
    css_class = 'form clearfix'
    submit_text = None
    show_children_errors = True

    fields = [
        PasswordField('current_password', label_text=N_('Current password'), maxlength=80, 
            validator=ValidPassword(not_empty=True)),
        PasswordField('new_password', label_text=N_('New password'), maxlength=80, 
            validator=NotEmpty, attrs={'autocomplete': 'off'}),
        PasswordField('confirm_password', label_text=N_('Confirm password'), maxlength=80, 
            validator=NotEmpty, attrs={'autocomplete': 'off'}),
        
        TextLinkButton('cancel', link='/profile', default=N_('Cancel'), css_classes=['btn-cancel']),
        SubmitButton('save', default=N_('Change Password'), css_classes=['mcore-btn', 'btn-submit']),
    ]
    
    validator = Schema(chained_validators=(
        FieldsMatch('new_password', 'confirm_password', 
            messages={'invalidNoMatch': N_("Passwords do not match"),}),
    ))

    def post_init(self, *args, **kwargs):
        events.PasswordChangeForm(self)


