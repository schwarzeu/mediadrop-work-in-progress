# This file is a part of MediaCore CE, Copyright 2009-2012 MediaCore Inc.
# The source code contained in this file is licensed under the GPL.
# See LICENSE.txt in the main project directory, for more information.
#
# Copyright (c) 2012 Felix Schwarz (www.schwarz.eu)

from datetime import datetime, timedelta

from formencode import Invalid
from tw.forms import HiddenField, PasswordField
from tw.forms.validators import (FancyValidator, FieldsMatch, FormValidator, 
    NotEmpty, Schema)
    
from mediacore.forms import ListForm, TextField, SubmitButton, email_validator
from mediacore.forms.profile import TextLinkButton
from mediacore.model import DBSession, User
from mediacore.lib.i18n import _, N_
from mediacore.plugin import events


__all__ = ['ForgotPasswordForm', 'ResetPasswordForm']

class ForgotPasswordForm(ListForm):
    template = 'forms/box_form.html'
    method = 'POST'
    id = 'forgot-password-form'
    css_class = 'form clearfix'
    submit_text = None
    show_children_errors = True

    fields = [
        TextField('email', label_text=N_('Email address'), 
            validator=email_validator(not_empty=True),
            # 'autofocus' is actually not XHTML-compliant
            attrs={'autofocus': True}),
        
        SubmitButton('forgot_password_button', default=N_('Reset Password'), 
            css_classes=['mcore-btn', 'btn-submit', 'f-rgt']),
        TextLinkButton('login', default=N_('Login'), 
            link='/login'),
    ]

    def post_init(self, *args, **kwargs):
        events.ForgotPasswordForm(self)


class PasswordHashValidator(FormValidator):
    hash_field = None
    email_field = None
    __unpackargs__ = ('hash_field', 'email_field')
    
    hash_expires_in = timedelta(days=1)
    
    def validate_python(self, values, state):
        email = values.get(self.email_field)
        user = User.by_email_address(email)
        if not user:
            self._raise_error(_('Unknown user for email address.'), 
                self.email_field, email, state)
        if not self._is_hash_valid_for_user(values.get(self.hash_field), user):
            self._raise_error(_('Invalid hash provided for email address.'), 
                self.hash_field, email, state)
    
    def _is_hash_valid_for_user(self, hash_, user):
        reset_data = user.data.get('password_reset')
        if (not reset_data) or (not reset_data.get('hash')):
            return False
        if reset_data['hash'] != hash_:
            return False
        hash_created_on = datetime.utcfromtimestamp(reset_data.get('created_on', 0))
        is_hash_expired = (hash_created_on + self.hash_expires_in > datetime.utcnow())
        return is_hash_expired
    
    def _raise_error(self, message, field_name, value, state):
        e = Invalid(message, value, state)
        raise Invalid(message, value, state, error_dict={field_name: e})


class ValidResetPasswordHashSchema(Schema):
    # when fallback from 'reset_password()' -> 'confirm_reset()' occurs, let's
    # ignore the extra fields.
    allow_extra_fields=True
    
    hash_ = FancyValidator(not_empty=True, if_missing='')
    email = FancyValidator(not_empty=True, if_missing='')
    
    chained_validators = (
        PasswordHashValidator('hash_', 'email'),
    )
    

class ResetPasswordForm(ListForm):
    template = 'forms/box_form.html'
    method = 'POST'
    id = 'reset-password-form'
    css_class = 'form clearfix'
    submit_text = None
    show_children_errors = True

    fields = [
        HiddenField('hash_'),
        HiddenField('email'),
    
        PasswordField('new_password', label_text=N_('New password'), maxlength=80, 
            validator=NotEmpty, attrs={'autocomplete': 'off', 'autofocus': True}),
        PasswordField('confirm_password', label_text=N_('Confirm password'), maxlength=80, 
            validator=NotEmpty, attrs={'autocomplete': 'off'}),
        
        SubmitButton('reset_password_button', default=N_('Set Password'), 
            css_classes=['mcore-btn', 'btn-submit', 'f-rgt']),
    ]

    validator = Schema(chained_validators=(
        PasswordHashValidator('hash_', 'email'),
        FieldsMatch('new_password', 'confirm_password', 
            messages={'invalidNoMatch': N_("Passwords do not match"),}),
    ))

    def post_init(self, *args, **kwargs):
        events.ResetPasswordForm(self)

